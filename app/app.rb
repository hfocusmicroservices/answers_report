require_relative 'task/answer'

updater = Answer.new

if updater.start_process
  puts "Processo concluído com sucesso!"
  exit 0
else
  puts "Falha durante o processo. Verifique os LOGs para mais informações!"
end