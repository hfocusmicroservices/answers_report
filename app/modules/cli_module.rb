require 'date'

require_relative '../configs/constants'

# Basic setup for all process must be defined here
class CLI

  # @constructor
  def initialize
    define_env
    define_args
    validate_args
  end

  # @return datetime
  def retrieve_period_start
    @s_period
  end

  # @return datetime
  def retrieve_period_end
    @e_period
  end

  # @return String
  def retrieve_customer
    @customer
  end

  # @return String
  def retrieve_email
    @email
  end

  # @return String
  def retrieve_env
    @env
  end

  # @return String
  def retrieve_questionary
    @questionary
  end


  private

  # @void
  def define_default customer

    @customer = customer.to_i
    now = Date.today
    yesterday = now.prev_day

    now = now.prev_day

    @s_period = DateTime.new(now.year, now.month, now.day, 0, 0, 0)
    @e_period = DateTime.new(yesterday.year, yesterday.month, yesterday.day, 23, 59, 59)

    nil
  end

  # @void
  def define_env
    @env = ENVIRONMENT
    nil
  end

  # @void
  def define_customer(customer = 0)
    @customer = customer.to_i
    nil
  end

  # @void
  def define_period_start period
    @s_period = DateTime.new(period.year, period.month, period.day, 0, 0, 0)
    nil
  end

  # @void
  def define_period_end period
    @e_period = DateTime.new(period.year, period.month, period.day, 23, 59, 59)
    nil
  end

  # @void
  def define_email_report address
    @email = address
    nil
  end

  # @void
  def define_questionary questionary
    @questionary = questionary
    nil
  end

  # @void
  def check_param param

    param = param.split('=')

    case param[0]
    when '--customer'
      define_customer param[1]
    when '--start-period'
      define_period_start Date.parse(param[1])
    when '--end-period'
      define_period_end Date.parse(param[1])
    when '--send-log-to'
      define_email_report param[1]
    when '--questionary'
      define_questionary param[1]
    end

    nil
  end

  # @void
  def define_args

    args = ARGV
    if args.count > 0
      total = args.count
      i = 0

      while i < total
        if !args[i].nil?
          check_param args[i]
        else
          break
        end
        i = i + 1
      end
    end

    nil
  end

  # @void
  def validate_args

    if retrieve_customer.nil?
      define_customer 0
    end

    if retrieve_period_start.nil? || retrieve_period_end.nil?
      define_default @customer
    end

    nil
  end
end