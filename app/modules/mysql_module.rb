require 'mysql2'

require_relative '../configs/constants'
require_relative 'cli_module'

# This class connect the entire system to mysql database 'Maestro'
class MysqlMaestro

  # @constructor
  def initialize
    @cli = CLI.new
    @env = @cli.retrieve_env
    @env == 'prod' ? define_db_production_setup : define_db_hmg_setup
  end

  # from: ReturnUpdater.start_process
  # @return array
  def get_questions q

    sql = "SELECT * FROM #{M_QUESTIONARY}"
    sql = "#{sql} WHERE #{Q} = #{q}"
    sql = "#{sql};"

    process = "retrieve_sent_log. Parametros: #{q}"
    result = run_query sql

    i = 0

    r = result.first
    
    r.each do |c|
      if !r["P#{i + 1}"].nil?
        i = i + 1
      end
    end

    i
  end

  # from: ReturnUpdater.start_process
  # @return array
  def get_answers customer, p_start, p_end, q

    keys = "bp.#{P}, br.P1, br.P2, br.P3, br.P4, br.P5, br.P6, br.P7, br.P8, br.P9, br.P10, br.P11, br.P12, br.P13, br.P14, br.P15, br.P16, br.P17, br.P18, br.P19, br.P20, br.P21, br.P22, br.P23, br.P24, br.P25, br.P26, br.P27, br.P28, br.P29, br.P30, br.P31, br.P32, br.P33, br.P34, br.P35, br.P36, br.P37, br.P38, br.P39, br.P40, br.P41, br.P42, br.P43, br.P44, br.P45, br.P46, br.P47, br.P48, br.P49, br.P50, br.P51, br.P52, br.P53, br.P54, br.P55, br.P56, br.P57, br.P58, br.P59, br.P60, br.P61, br.P62, br.P63, br.P64, br.P65, br.P66, br.P67, br.P68, br.P69, br.P70, br.P71, br.P72, br.P73, br.P74, br.P75, br.P76, br.P77, br.P78, br.P79, br.P80, br.P81, br.P82, br.P83, br.P84, br.P85, br.P86, br.P87, br.P88, br.P89, br.P90, br.P91, br.P92, br.P93, br.P94, br.P95, br.P96, br.P97, br.P98, br.P99, br.P100, br.P101, br.P102, br.P103, br.P104, br.P105, br.P106, br.P107, br.P108, br.P109, br.P110, br.P111, br.P112, br.P113, br.P114, br.P115, br.P116, br.P117, br.P118, br.P119, br.P120, br.P121, br.P122, br.P123, br.P124, br.P125, br.P126, br.P127, br.P128, br.P129, br.P130, br.P131, br.P132, br.P133, br.P134, br.P135, br.P136, br.P137, br.P138, br.P139, br.P140, br.P141, br.P142, br.P143, br.P144, br.P145, br.P146, br.P147, br.P148, br.P149, br.P150"

    sql = "SELECT #{keys} FROM #{M_PATIENTS} bp"
    sql = "#{sql} INNER JOIN #{M_ANSWERS} br ON bp.#{P} = br.#{P}"
    sql = "#{sql} WHERE br.ID_Cliente = #{customer}"
    sql = "#{sql} AND bp.#{Q} = '#{q}'"
    sql = "#{sql} AND DATE(bp.#{B}) BETWEEN '#{p_start}' AND '#{p_end}'"
    sql = "#{sql};"

    process = "retrieve_sent_log. Parametros: #{customer}, #{p_start}, #{p_end}"
    result = run_query sql

    result
  end

  # Close mysql connection if there's any
  # @void
  def close_connection

    if !@client.nil? 
      @client.close
    end

    nil
  end


  private

  # from: Mysql.run_query
  # @void
  def define_db_production_setup

    db = [MAESTRO_HOST, MAESTRO_USER, MAESTRO_PASS, MAESTRO_DB]
    @client = Mysql2::Client.new(
      host: db[0], username: db[1], password: db[2], database: db[3]
    )

    nil
  end

  # from: Mysql.run_query
  # @void
  def define_db_hmg_setup
    db = [HMG_MAESTRO_HOST, HMG_MAESTRO_USER, HMG_MAESTRO_PASS, HMG_MAESTRO_DB]
    @client = Mysql2::Client.new(
      host: db[0], username: db[1], password: db[2], database: db[3]
    )

    nil
  end

  # @return Dynamic Array
  def run_query sql

    begin
      result = @client.query sql
    rescue StandardError => e
      # log_error e, customer, process, interviewed, email
    end

    result
  end
end