require 'pony'

# This class should receive subject, message and address and then send e-mails
class SenderModule

  # @constructor
  def initialize
    define_configuration
  end

  # @void
  def define_message message
    @message = message
    nil
  end

  # @void
  def define_subject subject
    @subject = subject
    nil
  end

  # @void
  def define_to_address address
    @to_address = address
    nil
  end

  # Send e-mail
  # @return boolean
  def send_email

    define_configuration

    if Pony.mail(to: @to_address, via: :smtp, subject: @subject, body: @message, via_options: {
        address: 'smtp.gmail.com',
        port: '587',
        user_name: @setup[0],
        password: @setup[1],
        authentication: :plain,
        domain: 'hfocus.com.br' })
      true
    end
    false
  end


  private

  # @void
  def define_configuration
    @setup = [DEV_EMAIL, MAIL_PASS]
    nil
  end
end
