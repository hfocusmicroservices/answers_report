require 'progress_bar'

require_relative 'main'
require_relative '../modules/cli_module'
require_relative '../modules/mysql_module'
require_relative '../modules/sender_module'

# Some cool stuff here
class Answer < Main

  # Constructor method
  def initialize
    @cli = CLI.new
    @mysql = MysqlMaestro.new
  end

  # from: app (no class or method)
  # @return boolean
  def start_process

    customer = @cli.retrieve_customer

    p_start = @cli.retrieve_period_start
    p_end = @cli.retrieve_period_end
    q = @cli.retrieve_questionary

    total = @mysql.get_questions q
    collection = @mysql.get_answers customer, p_start, p_end, q
    
    i = 0
    position = 1
    questionary = []

    collection.each do |c|
      while position <= total
        if c.has_key? ("P#{position}")
          if !c["P#{position}"].nil?
            position = position + 1
          else
            break
          end
        end
      end

      questionary[i] = position > total ? total : position - 1

      # Descomentar se necessário debugging
      # puts "#{i} respondeu até: #{questionary[i]}"

      position = 1
      i = i + 1
    end

    if questionary.nil?
      j = 0
    else
      j = questionary.min
    end

    if j.nil?
      puts "O questionário informado possui 0 respostas"
    else
      puts "O questionário informado possui #{total} perguntas"
      puts "#{questionary.count} encontrados para o questionário informado"

      while j <= total
        i = 0
        questionary.each do |c|
          if j == c
            i = i + 1
          end
        end

        # Descomentar se necessário debugging
        if i > 0
          puts "-- #{i} pessoas foram até: #{j}"
        end

        j = j + 1
      end  
    end

    true
  end
end