require_relative '../configs/constants'
require_relative '../modules/cli_module'
require_relative '../modules/sender_module'

# This class keeps all common methods between tasks
class Main

  # @require array result
  # @void
  def define_result result
    @result = result
    nil
  end

  # @return array
  def retrieve_results
    @result
  end

  # @void
  def define_email_setup(subject, message, email = nil)

    @sender = SenderModule.new

    address = email.nil? ? DEV_EMAIL : email

    @sender.define_to_address address
    @sender.define_subject subject
    @sender.define_message message

    nil
  end

  def send_notification
    @sender.send_email
  end
end